export default {
  APP_URL: process.env.APP_URL,
  API_URL: process.env.API_URL,
  HOTJAR_SITE_ID: process.env.HOTJAR_SITE_ID,
};
