import styled from 'styled-components';
import { compose, typography, space, color } from 'styled-system';
import { default as BootstrapButton } from 'react-bootstrap/Button';

export const Button = styled(BootstrapButton)`
  ${compose(typography, space, color)}
`;
