import React from 'react';
import { Form, FormControl, FormGroup, FormLabel } from 'components/Form';
import { Button } from 'components/Button';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export const LoginPage = () => {
  return (
    <Container className="min-vh-100 d-flex flex-column justify-content-center">
      <Row>
        <Col sm={{ span: 6, offset: 3 }} xl={{ span: 4, offset: 4 }}>
          <Card>
            <Card.Header>Login</Card.Header>
            <Card.Body>
              <Form>
                <FormGroup controlId="email">
                  <FormLabel>Email</FormLabel>
                  <FormControl type="email" placeholder="Email" />
                </FormGroup>
                <FormGroup controlId="password">
                  <FormLabel>Password</FormLabel>
                  <FormControl type="password" placeholder="Password" />
                </FormGroup>
                <Button>Login</Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
