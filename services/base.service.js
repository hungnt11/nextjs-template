import axios from 'axios';
import env from 'utils/env';
import { ApiError } from 'utils/error';

/**
 * Note that this will auto prepend our app's hostname and sets headers automatically
 */
export class BaseApiService {
  constructor() {
    this.instance = axios.create({
      baseURL: env.API_URL,
      headers: {
        'Content-Type': 'application/json',
      },
      withCredentials: true,
    });
  }

  post(url, data, options) {
    return this.handleRequest(this.instance.post(url, data, options));
  }

  get(url, options) {
    return this.handleRequest(this.instance.get(url, options));
  }

  put(url, data, options) {
    return this.handleRequest(this.instance.put(url, data, options));
  }

  delete(url, options) {
    return this.handleRequest(this.instance.delete(url, options));
  }

  /**
   * Handle generic error and transform response to app response.
   * @param axiosResponse
   * @returns {{data: *, originResponse: *}}
   */
  async handleRequest(axiosResponse) {
    try {
      const response = await axiosResponse;
      return {
        data: response.data,
        originResponse: response,
      };
    } catch (error) {
      let apiError = {
        name: 'NETWORK_ERROR',
        originError: null,
        code: 'UNKNOWN_ERROR',
        message: 'There is something wrong in server!',
      };
      apiError.originError = error;
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        const data = error.response.data;
        if (data) {
          apiError.message =
            data.message || 'There is something wrong in server!';
        }
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        apiError.code = 'NO_RESPONSE';
        apiError.message = 'Unable to connect to server';
      } else {
        // Something happened in setting up the request that triggered an Error
        apiError.code = 'RUNTIME_ERROR';
        apiError.message = error.message;
      }
      throw new ApiError(apiError);
    }
  }
}
