import { toast } from 'react-toastify';

/**
 * A wrapper for toast.
 */
class ToastAlertFactory {
  error(message, options) {
    toast.error(message, options);
  }

  warn(message, options) {
    toast.warn(message, options);
  }

  info(message, options) {
    toast.info(message, options);
  }

  success(message, options) {
    toast.success(message, options);
  }
}

export const ToastAlert = new ToastAlertFactory();
